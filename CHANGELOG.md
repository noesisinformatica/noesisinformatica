# Git Changelog for noesisinformatica
Changelog auto generated from Git commits.
## 2.7.4
### No issue

[eecf8de76efd190](https://github.com/noesisinformatica/noesisinformatica/commit/eecf8de76efd190) Jay Kola *2019-03-30 20:12:37*

chore: Removed snapshot from version number before release

[2c78fd85890bdd1](https://github.com/noesisinformatica/noesisinformatica/commit/2c78fd85890bdd1) Jay Kola *2019-03-30 20:10:40*

chore: Updated repositories to use https endpoints

[dde4b3d671e90e4](https://github.com/noesisinformatica/noesisinformatica/commit/dde4b3d671e90e4) Jay Kola *2019-03-30 18:08:34*

fix: Updated names of repositories to match the entries in settings.xml

[a7aa5754b16aed6](https://github.com/noesisinformatica/noesisinformatica/commit/a7aa5754b16aed6) Jay Kola *2016-08-31 18:33:58*

updating develop poms to master versions to avoid merge conflicts

[5a0b8f54ec86b80](https://github.com/noesisinformatica/noesisinformatica/commit/5a0b8f54ec86b80) Jay Kola *2016-08-31 18:33:58*

Updating develop poms back to pre merge state

[82b3c94dfb52123](https://github.com/noesisinformatica/noesisinformatica/commit/82b3c94dfb52123) Jay Kola *2016-08-31 18:33:22*

updating poms for 2.7.4-SNAPSHOT development


## 2.7.3
### No issue

[1b5b64446451e9f](https://github.com/noesisinformatica/noesisinformatica/commit/1b5b64446451e9f) Jay Kola *2016-08-31 18:33:35*

updating poms for branch'release/2.7.3' with non-snapshot versions

[03772fc5a6a7e1d](https://github.com/noesisinformatica/noesisinformatica/commit/03772fc5a6a7e1d) Jay Kola *2016-08-31 18:32:34*

Updated pom to include noesis public plugin repos

[da4a3d8ab7a0006](https://github.com/noesisinformatica/noesisinformatica/commit/da4a3d8ab7a0006) Jay Kola *2016-08-30 16:15:22*

chore: Added git id plugin that auto includes git properties in build folder

[b22717ac3cab618](https://github.com/noesisinformatica/noesisinformatica/commit/b22717ac3cab618) Jay Kola *2016-08-26 10:27:46*

Updating develop poms back to pre merge state

[acb05fd9dc1fab7](https://github.com/noesisinformatica/noesisinformatica/commit/acb05fd9dc1fab7) Jay Kola *2016-08-26 10:27:46*

updating develop poms to master versions to avoid merge conflicts

[a11106554d72454](https://github.com/noesisinformatica/noesisinformatica/commit/a11106554d72454) Jay Kola *2016-08-26 10:27:27*

updating poms for 2.7.3-SNAPSHOT development


## 2.7.2
### No issue

[05ef5d79ec8dccb](https://github.com/noesisinformatica/noesisinformatica/commit/05ef5d79ec8dccb) Jay Kola *2016-08-26 10:27:38*

updating poms for branch'release/2.7.2' with non-snapshot versions

[a0b807022f86dd2](https://github.com/noesisinformatica/noesisinformatica/commit/a0b807022f86dd2) Jay Kola *2016-08-26 10:26:53*

chore: Migrated to new termlex repos

[913cc6585272c17](https://github.com/noesisinformatica/noesisinformatica/commit/913cc6585272c17) Jay Kola *2016-08-26 10:22:38*

Updating develop poms back to pre merge state

[0ab6dbf575d3261](https://github.com/noesisinformatica/noesisinformatica/commit/0ab6dbf575d3261) Jay Kola *2016-08-26 10:22:38*

updating develop poms to master versions to avoid merge conflicts

[0d9cb9f682f0f24](https://github.com/noesisinformatica/noesisinformatica/commit/0d9cb9f682f0f24) Jay Kola *2016-08-26 09:56:32*

updating poms for 2.7.2-SNAPSHOT development


## 2.7.1
### No issue

[978e41a58c08066](https://github.com/noesisinformatica/noesisinformatica/commit/978e41a58c08066) Jay Kola *2016-08-26 09:56:43*

updating poms for branch'release/2.7.1' with non-snapshot versions

[2547c4aeaa486e3](https://github.com/noesisinformatica/noesisinformatica/commit/2547c4aeaa486e3) Jay Kola *2016-08-26 09:55:44*

feat: Moved changelog template to inline form so we don't need template in all projects

[af92092857db8a8](https://github.com/noesisinformatica/noesisinformatica/commit/af92092857db8a8) Jay Kola *2016-08-20 20:06:22*

updating poms for 2.7.1-SNAPSHOT development

[8cfdc90bd77b5bb](https://github.com/noesisinformatica/noesisinformatica/commit/8cfdc90bd77b5bb) Jay Kola *2016-08-20 20:05:54*

Updated license url to gitlab location

[ab23a873b736576](https://github.com/noesisinformatica/noesisinformatica/commit/ab23a873b736576) Jay Kola *2016-08-20 19:25:52*

updating develop poms to master versions to avoid merge conflicts

[8447de19ebee272](https://github.com/noesisinformatica/noesisinformatica/commit/8447de19ebee272) Jay Kola *2016-08-20 19:25:52*

Updating develop poms back to pre merge state

[e31a9cf3eabf557](https://github.com/noesisinformatica/noesisinformatica/commit/e31a9cf3eabf557) Jay Kola *2016-08-20 19:25:31*

updating poms for 2.7.0-SNAPSHOT development


## 2.6.0
### No issue

[e2b71f615d0acf3](https://github.com/noesisinformatica/noesisinformatica/commit/e2b71f615d0acf3) Jay Kola *2016-08-20 19:25:43*

updating poms for branch'release/2.6.0' with non-snapshot versions

[6b919fd5546fc97](https://github.com/noesisinformatica/noesisinformatica/commit/6b919fd5546fc97) Jay Kola *2016-08-20 19:24:51*

Updated plugin repositoriees

[4f5234dc62de79d](https://github.com/noesisinformatica/noesisinformatica/commit/4f5234dc62de79d) Jay Kola *2016-08-16 08:05:09*

Updated enforcer plugin version and added test for jdk 1.8

[c7708f7d0f57586](https://github.com/noesisinformatica/noesisinformatica/commit/c7708f7d0f57586) Jay Kola *2016-08-10 22:48:07*

Updating develop poms back to pre merge state

[4741e0fb4239dff](https://github.com/noesisinformatica/noesisinformatica/commit/4741e0fb4239dff) Jay Kola *2016-08-10 22:48:07*

updating develop poms to master versions to avoid merge conflicts

[1c6cc2055cf5e7a](https://github.com/noesisinformatica/noesisinformatica/commit/1c6cc2055cf5e7a) Jay Kola *2016-08-10 22:47:41*

updating poms for 2.6.0-SNAPSHOT development


## 2.5.0
### No issue

[3f9f6cb4b415ee7](https://github.com/noesisinformatica/noesisinformatica/commit/3f9f6cb4b415ee7) Jay Kola *2016-08-10 22:47:54*

updating poms for branch'release/2.5.0' with non-snapshot versions

[587f3b0b839275a](https://github.com/noesisinformatica/noesisinformatica/commit/587f3b0b839275a) Jay Kola *2016-08-10 22:46:46*

Updated project to use noesis maven repo

[fb6321fb0aab76d](https://github.com/noesisinformatica/noesisinformatica/commit/fb6321fb0aab76d) Jay Kola *2016-04-10 22:17:20*

Updating develop poms back to pre merge state

[3d1abcae1dead62](https://github.com/noesisinformatica/noesisinformatica/commit/3d1abcae1dead62) Jay Kola *2016-04-10 22:17:20*

updating develop poms to master versions to avoid merge conflicts

[3297cd1003fc5fd](https://github.com/noesisinformatica/noesisinformatica/commit/3297cd1003fc5fd) Jay Kola *2016-04-10 22:16:54*

updating poms for 2.5.0-SNAPSHOT development


## 2.4.0
### No issue

[2afb056415651fc](https://github.com/noesisinformatica/noesisinformatica/commit/2afb056415651fc) Jay Kola *2016-04-10 22:17:10*

updating poms for branch'release/2.4.0' with non-snapshot versions

[0bda57cfe189839](https://github.com/noesisinformatica/noesisinformatica/commit/0bda57cfe189839) Jay Kola *2016-04-10 22:16:10*

feat: Updated bundle plugin to version 3.0.1 to resolve issues with JDK8

[58d9ee130959ca1](https://github.com/noesisinformatica/noesisinformatica/commit/58d9ee130959ca1) Jay Kola *2016-03-22 17:00:01*

updating develop poms to master versions to avoid merge conflicts

[45ede40b74ea5f2](https://github.com/noesisinformatica/noesisinformatica/commit/45ede40b74ea5f2) Jay Kola *2016-03-22 17:00:01*

Updating develop poms back to pre merge state

[9bc71a931e54655](https://github.com/noesisinformatica/noesisinformatica/commit/9bc71a931e54655) Jay Kola *2016-03-22 16:55:55*

updating poms for 2.4.0-SNAPSHOT development


## 2.3.0
### No issue

[d8e57269d7bea5e](https://github.com/noesisinformatica/noesisinformatica/commit/d8e57269d7bea5e) Jay Kola *2016-03-22 16:56:09*

updating poms for branch'release/2.3.0' with non-snapshot versions

[99d46d80a219feb](https://github.com/noesisinformatica/noesisinformatica/commit/99d46d80a219feb) Jay Kola *2016-03-22 16:55:16*

feat: Bower managed resources are now ignored by license plugin - avoiding corrupting of downloaded resources with noesis license

[4ee94b0748f5327](https://github.com/noesisinformatica/noesisinformatica/commit/4ee94b0748f5327) Jay Kola *2016-03-04 12:42:03*

updating develop poms to master versions to avoid merge conflicts

[36a07c7e95d803a](https://github.com/noesisinformatica/noesisinformatica/commit/36a07c7e95d803a) Jay Kola *2016-03-04 12:42:03*

Updating develop poms back to pre merge state

[1627e8be98f4c98](https://github.com/noesisinformatica/noesisinformatica/commit/1627e8be98f4c98) Jay Kola *2016-03-04 12:41:37*

updating poms for 2.3.0-SNAPSHOT development


## 2.2.0
### No issue

[62ea9b9c09f4730](https://github.com/noesisinformatica/noesisinformatica/commit/62ea9b9c09f4730) Jay Kola *2016-03-04 12:41:54*

updating poms for branch'release/2.2.0' with non-snapshot versions

[f9353c6c184081b](https://github.com/noesisinformatica/noesisinformatica/commit/f9353c6c184081b) Jay Kola *2016-03-04 12:41:10*

feat: Added ability to skip download of license files

[f648fe1a6e07c46](https://github.com/noesisinformatica/noesisinformatica/commit/f648fe1a6e07c46) Jay Kola *2016-03-04 12:31:35*

updating develop poms to master versions to avoid merge conflicts

[8e1799b3261df7e](https://github.com/noesisinformatica/noesisinformatica/commit/8e1799b3261df7e) Jay Kola *2016-03-04 12:31:35*

Updating develop poms back to pre merge state

[c35db0b7658d1d6](https://github.com/noesisinformatica/noesisinformatica/commit/c35db0b7658d1d6) Jay Kola *2016-03-04 12:31:06*

updating poms for 2.2.0-SNAPSHOT development


## 2.1.0
### No issue

[bf2a3e6f5d23ecc](https://github.com/noesisinformatica/noesisinformatica/commit/bf2a3e6f5d23ecc) Jay Kola *2016-03-04 12:31:25*

updating poms for branch'release/2.1.0' with non-snapshot versions

[0c1556ae6b29374](https://github.com/noesisinformatica/noesisinformatica/commit/0c1556ae6b29374) Jay Kola *2016-03-04 12:30:13*

fix: License file in base directory no longer gets updated with project version. Reduces unncesary change logs from update of license file.

[3ac2e1d78c0b03b](https://github.com/noesisinformatica/noesisinformatica/commit/3ac2e1d78c0b03b) Jay Kola *2016-03-04 09:51:03*

feat: Added ability to exclude aggregation of third party licenses

[9925f2a613bee83](https://github.com/noesisinformatica/noesisinformatica/commit/9925f2a613bee83) Jay Kola *2016-02-15 22:13:22*

Updating develop poms back to pre merge state

[61211c136882505](https://github.com/noesisinformatica/noesisinformatica/commit/61211c136882505) Jay Kola *2016-02-15 22:13:22*

updating develop poms to master versions to avoid merge conflicts

[a1686768e003c57](https://github.com/noesisinformatica/noesisinformatica/commit/a1686768e003c57) Jay Kola *2016-02-15 22:13:11*

updating poms for branch'release/2.0.0' with non-snapshot versions

[93c925c06932e6c](https://github.com/noesisinformatica/noesisinformatica/commit/93c925c06932e6c) Jay Kola *2016-02-15 22:12:48*

updating poms for 2.1.0-SNAPSHOT development

[23b0698f9dda715](https://github.com/noesisinformatica/noesisinformatica/commit/23b0698f9dda715) Jay Kola *2016-02-15 22:11:55*

Added gitflow plugin

[347308052d97ed2](https://github.com/noesisinformatica/noesisinformatica/commit/347308052d97ed2) Jay Kola *2015-10-01 17:31:11*

Removed virgo profile

[ff1f7b0826ff3b2](https://github.com/noesisinformatica/noesisinformatica/commit/ff1f7b0826ff3b2) Jay Kola *2015-10-01 17:28:25*

Adding gitignore

[982735c1a4278bf](https://github.com/noesisinformatica/noesisinformatica/commit/982735c1a4278bf) Jay Kola *2015-09-30 17:40:10*

Add short git version configuration for build number plugin

[573b8d370adec58](https://github.com/noesisinformatica/noesisinformatica/commit/573b8d370adec58) Jay Kola *2015-09-30 17:29:01*

Included github repo information and failsafe plugin default conf

[9663976dbab78ee](https://github.com/noesisinformatica/noesisinformatica/commit/9663976dbab78ee) Jay Kola *2015-09-11 14:35:09*

Updated with build status

[493c26eef522804](https://github.com/noesisinformatica/noesisinformatica/commit/493c26eef522804) Jay Kola *2015-09-11 14:31:41*

Added README

[f4b873e947f8a4f](https://github.com/noesisinformatica/noesisinformatica/commit/f4b873e947f8a4f) Jay Kola *2015-08-27 03:54:51*

Removed step for maven clean plugin which was deleting jar from target directory

[391907687ab457a](https://github.com/noesisinformatica/noesisinformatica/commit/391907687ab457a) Jay Kola *2015-08-26 20:28:48*

Updated license plugin to remove aggregate-third-party goal

[f5e714eba17d659](https://github.com/noesisinformatica/noesisinformatica/commit/f5e714eba17d659) Jay Kola *2015-08-26 18:21:23*

Updated various plugin versions and included property for skipping license plugin goals

[f8bafd2240c85fc](https://github.com/noesisinformatica/noesisinformatica/commit/f8bafd2240c85fc) Jay Kola *2014-08-07 17:48:50*

Added files updated by maven release;

[830b9a5557d8e95](https://github.com/noesisinformatica/noesisinformatica/commit/830b9a5557d8e95) Jay Kola *2014-08-07 17:48:39*

Added tag Start of version 1.9 development for changeset f08550fef996


## 2.0.0
### No issue

[d62961f16f95367](https://github.com/noesisinformatica/noesisinformatica/commit/d62961f16f95367) Jay Kola *2016-02-15 22:13:11*

updating poms for branch'release/2.0.0' with non-snapshot versions

[095045f4e39e93b](https://github.com/noesisinformatica/noesisinformatica/commit/095045f4e39e93b) Jay Kola *2016-02-15 22:11:55*

Added gitflow plugin

[b034d6b4274d301](https://github.com/noesisinformatica/noesisinformatica/commit/b034d6b4274d301) Jay Kola *2015-10-01 17:31:11*

Removed virgo profile

[81c00c4a79fbfb8](https://github.com/noesisinformatica/noesisinformatica/commit/81c00c4a79fbfb8) Jay Kola *2015-10-01 17:28:25*

Adding gitignore

[0a7fa2bcf171098](https://github.com/noesisinformatica/noesisinformatica/commit/0a7fa2bcf171098) Jay Kola *2015-09-30 17:40:10*

Add short git version configuration for build number plugin

[d66f9302c2b3c9c](https://github.com/noesisinformatica/noesisinformatica/commit/d66f9302c2b3c9c) Jay Kola *2015-09-30 17:29:01*

Included github repo information and failsafe plugin default conf

[e8a9292f5e8cbd3](https://github.com/noesisinformatica/noesisinformatica/commit/e8a9292f5e8cbd3) Jay Kola *2015-09-11 14:35:09*

Updated with build status

[90bf4b9d1cff42d](https://github.com/noesisinformatica/noesisinformatica/commit/90bf4b9d1cff42d) Jay Kola *2015-09-11 14:31:41*

Added README


## 1.9.0
### No issue

[c7411b16b358ad0](https://github.com/noesisinformatica/noesisinformatica/commit/c7411b16b358ad0) Jay Kola *2015-08-27 03:54:51*

Removed step for maven clean plugin which was deleting jar from target directory

[8c77bf4dff8876d](https://github.com/noesisinformatica/noesisinformatica/commit/8c77bf4dff8876d) Jay Kola *2015-08-26 20:28:48*

Updated license plugin to remove aggregate-third-party goal

[3dc619a1dbbb09e](https://github.com/noesisinformatica/noesisinformatica/commit/3dc619a1dbbb09e) Jay Kola *2015-08-26 18:21:23*

Updated various plugin versions and included property for skipping license plugin goals

[c41884ca0ce52e2](https://github.com/noesisinformatica/noesisinformatica/commit/c41884ca0ce52e2) Jay Kola *2014-08-07 17:48:50*

Added files updated by maven release;

[d3997c47394e07e](https://github.com/noesisinformatica/noesisinformatica/commit/d3997c47394e07e) Jay Kola *2014-08-07 17:48:39*

Added tag Start of version 1.9 development for changeset f08550fef996


## 1.8.0
### No issue

[62d4a4ee91e8ed4](https://github.com/noesisinformatica/noesisinformatica/commit/62d4a4ee91e8ed4) Jay Kola *2014-08-07 17:46:14*

Added tag Before release of version 1.8 for changeset 2d6cb4313854


## Start_of_version_1.9_development
### No issue

[a911a905ea67e69](https://github.com/noesisinformatica/noesisinformatica/commit/a911a905ea67e69) Jay Kola *2014-08-07 17:46:14*

Added tag Before release of version 1.8 for changeset 2d6cb4313854


## Before_release_of_version_1.8
### No issue

[ac6518efa263f64](https://github.com/noesisinformatica/noesisinformatica/commit/ac6518efa263f64) Jay Kola *2014-08-07 17:45:02*

Remove build helper plugin since it seems to cause problems with releases. Also included maven-info-reports plugin

[1b0401b96c23d33](https://github.com/noesisinformatica/noesisinformatica/commit/1b0401b96c23d33) Jay Kola *2014-05-19 21:46:18*

Added files updated by Maven

[556cff312c01848](https://github.com/noesisinformatica/noesisinformatica/commit/556cff312c01848) Jay Kola *2014-05-19 21:45:59*

Added tag Start of version 1.8 development for changeset 7a92e74f6709


## Before-release-of-version-1.8
### No issue

[02273fe38236d89](https://github.com/noesisinformatica/noesisinformatica/commit/02273fe38236d89) Jay Kola *2014-08-07 17:45:02*

Remove build helper plugin since it seems to cause problems with releases. Also included maven-info-reports plugin

[a35d692fe8a82c1](https://github.com/noesisinformatica/noesisinformatica/commit/a35d692fe8a82c1) Jay Kola *2014-05-19 21:46:18*

Added files updated by Maven

[1f2573f8e29f48a](https://github.com/noesisinformatica/noesisinformatica/commit/1f2573f8e29f48a) Jay Kola *2014-05-19 21:45:59*

Added tag Start of version 1.8 development for changeset 7a92e74f6709


## Start_of_version_1.8_development
### No issue

[b6953da0e1b29d6](https://github.com/noesisinformatica/noesisinformatica/commit/b6953da0e1b29d6) Jay Kola *2014-05-19 21:41:55*

Added tag Before release of version 1.7.1 for changeset 739fa04c327b


## 1.7.1
### No issue

[651781af43364f6](https://github.com/noesisinformatica/noesisinformatica/commit/651781af43364f6) Jay Kola *2014-05-19 21:41:55*

Added tag Before release of version 1.7.1 for changeset 739fa04c327b


## Before_release_of_version_1.7.1
### No issue

[f4fd191a0b8c04c](https://github.com/noesisinformatica/noesisinformatica/commit/f4fd191a0b8c04c) Jay Kola *2014-04-15 09:50:10*

case 705: INPROGRESS: Updated UMLdoclet plugin version to fix error with </h2>

[3418d20d8e7f548](https://github.com/noesisinformatica/noesisinformatica/commit/3418d20d8e7f548) Jay Kola *2014-04-12 18:50:15*

case 696: INPROGRESS: Moved more plugin versions to properties

[d97c311ab747bb7](https://github.com/noesisinformatica/noesisinformatica/commit/d97c311ab747bb7) Jay Kola *2014-04-04 11:56:34*

Added files updated by Maven release

[f35b5b0dd8098ee](https://github.com/noesisinformatica/noesisinformatica/commit/f35b5b0dd8098ee) Jay Kola *2014-04-04 11:56:23*

Added tag Start of version 1.7.1 development for changeset 5f82c7ebf90f


## Before-release-of-version-1.7.1
### No issue

[125ae0005b9645e](https://github.com/noesisinformatica/noesisinformatica/commit/125ae0005b9645e) Jay Kola *2014-04-15 09:50:10*

case 705: INPROGRESS: Updated UMLdoclet plugin version to fix error with </h2>

[ba32507e5242bbb](https://github.com/noesisinformatica/noesisinformatica/commit/ba32507e5242bbb) Jay Kola *2014-04-12 18:50:15*

case 696: INPROGRESS: Moved more plugin versions to properties

[4471fd59c0bd3fa](https://github.com/noesisinformatica/noesisinformatica/commit/4471fd59c0bd3fa) Jay Kola *2014-04-04 11:56:34*

Added files updated by Maven release

[6911ed60e7ee9b6](https://github.com/noesisinformatica/noesisinformatica/commit/6911ed60e7ee9b6) Jay Kola *2014-04-04 11:56:23*

Added tag Start of version 1.7.1 development for changeset 5f82c7ebf90f


## Start_of_version_1.7.1_development
### No issue

[52c43e6d5b3d459](https://github.com/noesisinformatica/noesisinformatica/commit/52c43e6d5b3d459) Jay Kola *2014-04-04 11:41:28*

Added tag Before release of version 1.7 for changeset 8af417fb1e7a


## 1.7.0
### No issue

[c8887dc2f115623](https://github.com/noesisinformatica/noesisinformatica/commit/c8887dc2f115623) Jay Kola *2014-04-04 11:41:28*

Added tag Before release of version 1.7 for changeset 8af417fb1e7a


## Before_release_of_version_1.7
### No issue

[45d128b5947d192](https://github.com/noesisinformatica/noesisinformatica/commit/45d128b5947d192) Jay Kola *2014-04-04 11:41:03*

case 696: INPROGRESS: Excluded blueprint profile from Manifest

[d2b3d09b4bbebce](https://github.com/noesisinformatica/noesisinformatica/commit/d2b3d09b4bbebce) Jay Kola *2014-04-04 11:39:51*

case 696: INPROGRESS: Made OSGi the default build profile

[30d46c14f54105d](https://github.com/noesisinformatica/noesisinformatica/commit/30d46c14f54105d) Jay Kola *2014-03-31 09:10:54*

case 696: INPROGRESS: Added osgi and default profiles that build osgi bundles or jars respectively

[d8f7fb704c8dc8f](https://github.com/noesisinformatica/noesisinformatica/commit/d8f7fb704c8dc8f) Jay Kola *2014-03-20 15:15:56*

case 679: INPROGRESS: Added Noesis repo to plugin repos, so we can use Noesis license artefact

[3ad4fcc600f27eb](https://github.com/noesisinformatica/noesisinformatica/commit/3ad4fcc600f27eb) Jay Kola *2014-03-20 13:42:13*

case 679: INPROGRESS: Added Noesis repo to POM so we can use Noesis license artefact, which is deployed on Noesis repo

[7a84517382dd663](https://github.com/noesisinformatica/noesisinformatica/commit/7a84517382dd663) Jay Kola *2014-03-20 13:41:29*

case 679: INPROGRESS: Updated version to 1.6.1, so we can add fix

[1dc885ba15a3119](https://github.com/noesisinformatica/noesisinformatica/commit/1dc885ba15a3119) Jay Kola *2014-03-20 13:39:26*

Added tag Start of version 1.6.1 that includes fix for case 679 for changeset 45774cc27f18

[7523a6734ac65ab](https://github.com/noesisinformatica/noesisinformatica/commit/7523a6734ac65ab) Jay Kola *2014-03-20 00:44:46*

Adding files updated by Maven release

[fe6a5093cddb4b0](https://github.com/noesisinformatica/noesisinformatica/commit/fe6a5093cddb4b0) Jay Kola *2014-03-20 00:44:22*

Added tag Start of version 1.7 development for changeset 217dd845701a


## Before-release-of-version-1.7
### No issue

[43361de29ce83b9](https://github.com/noesisinformatica/noesisinformatica/commit/43361de29ce83b9) Jay Kola *2014-04-04 11:41:03*

case 696: INPROGRESS: Excluded blueprint profile from Manifest

[d18d4d2b6b314ae](https://github.com/noesisinformatica/noesisinformatica/commit/d18d4d2b6b314ae) Jay Kola *2014-04-04 11:39:51*

case 696: INPROGRESS: Made OSGi the default build profile

[dc95446876bc0c1](https://github.com/noesisinformatica/noesisinformatica/commit/dc95446876bc0c1) Jay Kola *2014-03-31 09:10:54*

case 696: INPROGRESS: Added osgi and default profiles that build osgi bundles or jars respectively

[870e6b23b11c3b7](https://github.com/noesisinformatica/noesisinformatica/commit/870e6b23b11c3b7) Jay Kola *2014-03-20 15:15:56*

case 679: INPROGRESS: Added Noesis repo to plugin repos, so we can use Noesis license artefact

[700e1a1ff1218f4](https://github.com/noesisinformatica/noesisinformatica/commit/700e1a1ff1218f4) Jay Kola *2014-03-20 13:42:13*

case 679: INPROGRESS: Added Noesis repo to POM so we can use Noesis license artefact, which is deployed on Noesis repo

[8c0901b54581872](https://github.com/noesisinformatica/noesisinformatica/commit/8c0901b54581872) Jay Kola *2014-03-20 13:41:29*

case 679: INPROGRESS: Updated version to 1.6.1, so we can add fix

[760a905e8e38b10](https://github.com/noesisinformatica/noesisinformatica/commit/760a905e8e38b10) Jay Kola *2014-03-20 13:39:26*

Added tag Start of version 1.6.1 that includes fix for case 679 for changeset 45774cc27f18

[6316dc4dd84711d](https://github.com/noesisinformatica/noesisinformatica/commit/6316dc4dd84711d) Jay Kola *2014-03-20 00:44:46*

Adding files updated by Maven release

[552b0bdf871669e](https://github.com/noesisinformatica/noesisinformatica/commit/552b0bdf871669e) Jay Kola *2014-03-20 00:44:22*

Added tag Start of version 1.7 development for changeset 217dd845701a


## Start_of_version_1.7_development
### No issue

[ec33b780aa2d64e](https://github.com/noesisinformatica/noesisinformatica/commit/ec33b780aa2d64e) Jay Kola *2014-03-20 00:41:01*

Added tag Before release of version 1.6 for changeset d9404c167ee1


## 1.6.0
### No issue

[81cc3e401b62665](https://github.com/noesisinformatica/noesisinformatica/commit/81cc3e401b62665) Jay Kola *2014-03-20 00:41:01*

Added tag Before release of version 1.6 for changeset d9404c167ee1


## Before_release_of_version_1.6
### No issue

[7d4c7ff76420787](https://github.com/noesisinformatica/noesisinformatica/commit/7d4c7ff76420787) Jay Kola *2014-03-20 00:39:10*

case 678: INPROGRESS: Moved all plugin versions to separate properties

[d0bcedfd976815d](https://github.com/noesisinformatica/noesisinformatica/commit/d0bcedfd976815d) Jay Kola *2014-03-19 20:31:55*

case 671: INPROGRESS: Updated license plugin to exclude gsp templates

[e12726bc7ba8b86](https://github.com/noesisinformatica/noesisinformatica/commit/e12726bc7ba8b86) Jay Kola *2014-03-05 19:55:13*

Adding files updated by Maven

[d097f46e74aea12](https://github.com/noesisinformatica/noesisinformatica/commit/d097f46e74aea12) Jay Kola *2014-03-05 19:54:48*

Added tag Start of version 1.6 development for changeset 2eee1263f45f


## Before-release-of-version-1.6
### No issue

[4562abae8406ac8](https://github.com/noesisinformatica/noesisinformatica/commit/4562abae8406ac8) Jay Kola *2014-03-20 00:39:10*

case 678: INPROGRESS: Moved all plugin versions to separate properties

[4dddaa17462f4cb](https://github.com/noesisinformatica/noesisinformatica/commit/4dddaa17462f4cb) Jay Kola *2014-03-19 20:31:55*

case 671: INPROGRESS: Updated license plugin to exclude gsp templates

[89e40b26c352863](https://github.com/noesisinformatica/noesisinformatica/commit/89e40b26c352863) Jay Kola *2014-03-05 19:55:13*

Adding files updated by Maven

[396129fb9468fea](https://github.com/noesisinformatica/noesisinformatica/commit/396129fb9468fea) Jay Kola *2014-03-05 19:54:48*

Added tag Start of version 1.6 development for changeset 2eee1263f45f


## Start_of_version_1.6_development
### No issue

[04014b11a145ce2](https://github.com/noesisinformatica/noesisinformatica/commit/04014b11a145ce2) Jay Kola *2014-03-05 19:52:17*

Updated license version to 1.2 release

[4d583f8199c9a34](https://github.com/noesisinformatica/noesisinformatica/commit/4d583f8199c9a34) Jay Kola *2014-03-05 19:51:13*

Adding file updated by Maven

[e592ba5140e03d7](https://github.com/noesisinformatica/noesisinformatica/commit/e592ba5140e03d7) Jay Kola *2014-03-05 19:51:03*

Added tag Start of version 1.5 development for changeset b2b870bc8da3


## 1.5.0
### No issue

[59eb82c577e41eb](https://github.com/noesisinformatica/noesisinformatica/commit/59eb82c577e41eb) Jay Kola *2014-03-05 19:52:17*

Updated license version to 1.2 release

[07c916e2fb9ce1e](https://github.com/noesisinformatica/noesisinformatica/commit/07c916e2fb9ce1e) Jay Kola *2014-03-05 19:51:13*

Adding file updated by Maven

[963bf0e7eaf9a76](https://github.com/noesisinformatica/noesisinformatica/commit/963bf0e7eaf9a76) Jay Kola *2014-03-05 19:51:03*

Added tag Start of version 1.5 development for changeset b2b870bc8da3


## Start_of_version_1.5_development
### No issue

[56973c6bf1bbb49](https://github.com/noesisinformatica/noesisinformatica/commit/56973c6bf1bbb49) Jay Kola *2014-03-05 19:47:40*

Added tag Before release of version 1.4 for changeset e6363e98882e


## 1.4.0
### No issue

[3f599b8cc1055b1](https://github.com/noesisinformatica/noesisinformatica/commit/3f599b8cc1055b1) Jay Kola *2014-03-05 19:47:40*

Added tag Before release of version 1.4 for changeset e6363e98882e


## Before-release-of-version-1.4
### No issue

[19041727573ba32](https://github.com/noesisinformatica/noesisinformatica/commit/19041727573ba32) Jay Kola *2014-03-05 19:47:24*

Adding license file updated by license plugin

[8bb2579ff60553b](https://github.com/noesisinformatica/noesisinformatica/commit/8bb2579ff60553b) Jay Kola *2014-03-05 19:46:27*

Renabled build helper after setting ti to only delete current version snapshots

[44fddc038c3399e](https://github.com/noesisinformatica/noesisinformatica/commit/44fddc038c3399e) Jay Kola *2014-03-05 19:45:42*

Added default license information for all Noesis Informatica projects

[c929d95754be4c3](https://github.com/noesisinformatica/noesisinformatica/commit/c929d95754be4c3) Jay Kola *2014-03-05 19:45:10*

Updated POM to use clean-plugin to delete existing license.txt file before license-plugin updates it. The maven replacer plugin then updates this license file.

[25cd11074adea9f](https://github.com/noesisinformatica/noesisinformatica/commit/25cd11074adea9f) Jay Kola *2014-03-04 21:15:07*

Added tag Start of version 1.4 development for changeset 6766094735a9


## Before_release_of_version_1.4
### No issue

[128dffc84ed0a22](https://github.com/noesisinformatica/noesisinformatica/commit/128dffc84ed0a22) Jay Kola *2014-03-05 19:47:24*

Adding license file updated by license plugin

[a80a122d4950f4e](https://github.com/noesisinformatica/noesisinformatica/commit/a80a122d4950f4e) Jay Kola *2014-03-05 19:46:27*

Renabled build helper after setting ti to only delete current version snapshots

[a46848cdb0cfec7](https://github.com/noesisinformatica/noesisinformatica/commit/a46848cdb0cfec7) Jay Kola *2014-03-05 19:45:42*

Added default license information for all Noesis Informatica projects

[d3347d1d505a8ee](https://github.com/noesisinformatica/noesisinformatica/commit/d3347d1d505a8ee) Jay Kola *2014-03-05 19:45:10*

Updated POM to use clean-plugin to delete existing license.txt file before license-plugin updates it. The maven replacer plugin then updates this license file.

[e6c3edeac1a2d04](https://github.com/noesisinformatica/noesisinformatica/commit/e6c3edeac1a2d04) Jay Kola *2014-03-04 21:15:07*

Added tag Start of version 1.4 development for changeset 6766094735a9


## Start_of_version_1.4_development
### No issue

[a5c85249b473e04](https://github.com/noesisinformatica/noesisinformatica/commit/a5c85249b473e04) Jay Kola *2014-03-04 21:12:22*

Updated license module to release version

[d07f26c6959cb47](https://github.com/noesisinformatica/noesisinformatica/commit/d07f26c6959cb47) Jay Kola *2014-03-04 21:05:44*

Added tag Before release of version 1.3 for changeset 966871799d6a


## 1.3.0
### No issue

[894db7247398020](https://github.com/noesisinformatica/noesisinformatica/commit/894db7247398020) Jay Kola *2014-03-04 21:12:22*

Updated license module to release version

[d090c171b72e531](https://github.com/noesisinformatica/noesisinformatica/commit/d090c171b72e531) Jay Kola *2014-03-04 21:05:44*

Added tag Before release of version 1.3 for changeset 966871799d6a


## Before_release_of_version_1.3
### No issue

[c15128451cad73b](https://github.com/noesisinformatica/noesisinformatica/commit/c15128451cad73b) Jay Kola *2014-03-04 21:05:05*

Removed the template license files since they are now provided via the license module

[e0b39bf3949b8d7](https://github.com/noesisinformatica/noesisinformatica/commit/e0b39bf3949b8d7) Jay Kola *2014-03-04 21:02:58*

Updated POM to include reference to updated license module and removed Build helper for the moment since it is causing issues with snapshot downloads

[34adacc06e4c248](https://github.com/noesisinformatica/noesisinformatica/commit/34adacc06e4c248) Jay Kola *2014-01-15 10:22:47*

Added tag Start of version 1.3 development for changeset ba1a71b69479


## Before-release-of-version-1.3
### No issue

[5f48245b64458d2](https://github.com/noesisinformatica/noesisinformatica/commit/5f48245b64458d2) Jay Kola *2014-03-04 21:05:05*

Removed the template license files since they are now provided via the license module

[5b774ea028e1a86](https://github.com/noesisinformatica/noesisinformatica/commit/5b774ea028e1a86) Jay Kola *2014-03-04 21:02:58*

Updated POM to include reference to updated license module and removed Build helper for the moment since it is causing issues with snapshot downloads

[a378af5eaa1a21a](https://github.com/noesisinformatica/noesisinformatica/commit/a378af5eaa1a21a) Jay Kola *2014-01-15 10:22:47*

Added tag Start of version 1.3 development for changeset ba1a71b69479


## Start_of_version_1.3_development
### No issue

[c21fb7ca8a7bf2f](https://github.com/noesisinformatica/noesisinformatica/commit/c21fb7ca8a7bf2f) Jay Kola *2014-01-15 08:20:36*

Added build helper to clear non-unique snapshots

[b854d44666621eb](https://github.com/noesisinformatica/noesisinformatica/commit/b854d44666621eb) Jay Kola *2014-01-15 08:19:43*

Reverted to cloudbees repos

[ce279e8ff571429](https://github.com/noesisinformatica/noesisinformatica/commit/ce279e8ff571429) Jay Kola *2014-01-14 04:08:05*

Added license plugin and templates

[04422a14d43f0f0](https://github.com/noesisinformatica/noesisinformatica/commit/04422a14d43f0f0) Jay Kola *2013-11-24 12:41:09*

Added a strict.semver.conformance property to control if builds fail when semver conformance is not met

[5bdc007f62b1904](https://github.com/noesisinformatica/noesisinformatica/commit/5bdc007f62b1904) Jay Kola *2013-11-24 10:29:30*

Updated buildNumber plugin to latest version

[4526d36effb0bb1](https://github.com/noesisinformatica/noesisinformatica/commit/4526d36effb0bb1) Jay Kola *2013-08-23 10:56:13*

Added profiles that control semver enforcer compatibility

[74743bcd1c89432](https://github.com/noesisinformatica/noesisinformatica/commit/74743bcd1c89432) Jay Kola *2013-07-29 14:31:34*

Added tag Start of version 1.1.1 development for changeset 456a941dcf28


## 1.2.0
### No issue

[02b9e2617900d8c](https://github.com/noesisinformatica/noesisinformatica/commit/02b9e2617900d8c) Jay Kola *2014-01-15 08:20:36*

Added build helper to clear non-unique snapshots

[6c3225e3bd809ab](https://github.com/noesisinformatica/noesisinformatica/commit/6c3225e3bd809ab) Jay Kola *2014-01-15 08:19:43*

Reverted to cloudbees repos

[9b6f6fbaf07733d](https://github.com/noesisinformatica/noesisinformatica/commit/9b6f6fbaf07733d) Jay Kola *2014-01-14 04:08:05*

Added license plugin and templates

[fb84570bec47d78](https://github.com/noesisinformatica/noesisinformatica/commit/fb84570bec47d78) Jay Kola *2013-11-24 12:41:09*

Added a strict.semver.conformance property to control if builds fail when semver conformance is not met

[51941ed05b653eb](https://github.com/noesisinformatica/noesisinformatica/commit/51941ed05b653eb) Jay Kola *2013-11-24 10:29:30*

Updated buildNumber plugin to latest version


## 1.1.1
### No issue

[ec4c62c3fedd1fe](https://github.com/noesisinformatica/noesisinformatica/commit/ec4c62c3fedd1fe) Jay Kola *2013-08-23 10:56:13*

Added profiles that control semver enforcer compatibility

[63a0ccf0ea3d6f3](https://github.com/noesisinformatica/noesisinformatica/commit/63a0ccf0ea3d6f3) Jay Kola *2013-07-29 14:31:34*

Added tag Start of version 1.1.1 development for changeset 456a941dcf28


## Start_of_version_1.1.1_development
### No issue

[fb655eb6c2820d5](https://github.com/noesisinformatica/noesisinformatica/commit/fb655eb6c2820d5) Jay Kola *2013-07-29 14:28:38*

Added snapshot tag in version number to facilitate release via maven

[64a19854950dfa4](https://github.com/noesisinformatica/noesisinformatica/commit/64a19854950dfa4) Jay Kola *2013-07-29 14:24:55*

case 257: FIXED: Warning now displayed when trying to deploy artefacts with snapshot dependencies

[280a986524d7c00](https://github.com/noesisinformatica/noesisinformatica/commit/280a986524d7c00) Jay Kola *2013-07-29 14:24:07*

case 255: FIXED: Maven version minimum 3.0 or greater is needed

[f6b5f2e9003b88e](https://github.com/noesisinformatica/noesisinformatica/commit/f6b5f2e9003b88e) Jay Kola *2013-07-29 14:23:09*

case 254: FIXED: Enforcer extra plugin used to verify presence of circular dependencies

[89e3e737af6503c](https://github.com/noesisinformatica/noesisinformatica/commit/89e3e737af6503c) Jay Kola *2013-07-29 14:21:42*

case 257: Added checks for semantic versioning in release artefacts -- done in verify phase

[211910c6241f120](https://github.com/noesisinformatica/noesisinformatica/commit/211910c6241f120) Developer Noesis Informatica *2013-03-04 23:06:38*

Updated license header wording

[f153a9a4ea3d0b1](https://github.com/noesisinformatica/noesisinformatica/commit/f153a9a4ea3d0b1) Jay Kola *2013-01-05 12:46:28*

Commneted out reference to web-dav plugin which was left active accidentally

[df69d4eb016023c](https://github.com/noesisinformatica/noesisinformatica/commit/df69d4eb016023c) Jay Kola *2013-01-05 12:41:03*

Moved jackrabbit config to extensions section - does not work if it is included as dependency of release plugin

[d2c104e7c3ecf5c](https://github.com/noesisinformatica/noesisinformatica/commit/d2c104e7c3ecf5c) Jay Kola *2013-01-05 12:02:15*

Replaced wagon-dav with wagon-webdav-jackrabbit plugin to fix Unbuffered Error

[6885920de6fd7f1](https://github.com/noesisinformatica/noesisinformatica/commit/6885920de6fd7f1) Jay Kola *2013-01-03 17:34:56*

Correct previous error - now plugin settings should be inherited in multi-module projects

[c3617c87b70137e](https://github.com/noesisinformatica/noesisinformatica/commit/c3617c87b70137e) Jay Kola *2013-01-03 17:29:38*

Specified that license header plugin settings inherited

[f5c559f23c082cd](https://github.com/noesisinformatica/noesisinformatica/commit/f5c559f23c082cd) Jay Kola *2013-01-03 16:56:58*

Moved plugins to pluginManagement for proper inheritance in multi-module projects

[26be97fa65cabd2](https://github.com/noesisinformatica/noesisinformatica/commit/26be97fa65cabd2) Noesis Developer *2012-12-02 15:48:29*

Added license header and package specification

[b91c7c1d5a5087e](https://github.com/noesisinformatica/noesisinformatica/commit/b91c7c1d5a5087e) Noesis Developer *2012-12-02 14:38:02*

Adding intial version of Noesis Informatica Organisation POM


## 1.1.0
### No issue

[86ce363726f40ab](https://github.com/noesisinformatica/noesisinformatica/commit/86ce363726f40ab) Jay Kola *2013-07-29 14:28:38*

Added snapshot tag in version number to facilitate release via maven

[d0afcae61de6e05](https://github.com/noesisinformatica/noesisinformatica/commit/d0afcae61de6e05) Jay Kola *2013-07-29 14:24:55*

case 257: FIXED: Warning now displayed when trying to deploy artefacts with snapshot dependencies

[f8d54cae5b454dd](https://github.com/noesisinformatica/noesisinformatica/commit/f8d54cae5b454dd) Jay Kola *2013-07-29 14:24:07*

case 255: FIXED: Maven version minimum 3.0 or greater is needed

[c265fef07f965e1](https://github.com/noesisinformatica/noesisinformatica/commit/c265fef07f965e1) Jay Kola *2013-07-29 14:23:09*

case 254: FIXED: Enforcer extra plugin used to verify presence of circular dependencies

[0ebfae2a65feb18](https://github.com/noesisinformatica/noesisinformatica/commit/0ebfae2a65feb18) Jay Kola *2013-07-29 14:21:42*

case 257: Added checks for semantic versioning in release artefacts -- done in verify phase

[5b040f2e7495c1e](https://github.com/noesisinformatica/noesisinformatica/commit/5b040f2e7495c1e) Developer Noesis Informatica *2013-03-04 23:06:38*

Updated license header wording

[0db0092cd1f87f9](https://github.com/noesisinformatica/noesisinformatica/commit/0db0092cd1f87f9) Jay Kola *2013-01-05 12:46:28*

Commneted out reference to web-dav plugin which was left active accidentally

[07a15aa37b0fbc5](https://github.com/noesisinformatica/noesisinformatica/commit/07a15aa37b0fbc5) Jay Kola *2013-01-05 12:41:03*

Moved jackrabbit config to extensions section - does not work if it is included as dependency of release plugin

[fb34842cd139d93](https://github.com/noesisinformatica/noesisinformatica/commit/fb34842cd139d93) Jay Kola *2013-01-05 12:02:15*

Replaced wagon-dav with wagon-webdav-jackrabbit plugin to fix Unbuffered Error

[6e9699efa5cc9df](https://github.com/noesisinformatica/noesisinformatica/commit/6e9699efa5cc9df) Jay Kola *2013-01-03 17:34:56*

Correct previous error - now plugin settings should be inherited in multi-module projects

[529a77e53e3987a](https://github.com/noesisinformatica/noesisinformatica/commit/529a77e53e3987a) Jay Kola *2013-01-03 17:29:38*

Specified that license header plugin settings inherited

[37bb0fa428d42ab](https://github.com/noesisinformatica/noesisinformatica/commit/37bb0fa428d42ab) Jay Kola *2013-01-03 16:56:58*

Moved plugins to pluginManagement for proper inheritance in multi-module projects

[5580b7797423f9f](https://github.com/noesisinformatica/noesisinformatica/commit/5580b7797423f9f) Noesis Developer *2012-12-02 15:48:29*

Added license header and package specification


